import os
import skimage.transform as trans
# import skimage.io as io
import matplotlib.pyplot as plt
import numpy as np

from PIL import Image
from skimage import data, io, filters
from scipy import ndimage
from sklearn import manifold, datasets

# Use gen_img just to crop ct image to same size as label
# Use gen_labels_black_and_white to get lungs labelled as white
# Use gen_labels_left_and_right to get right lung and left lung as red and blue

img_location = "data/Test/Image"

def png_to_tiff():
    path = img_location
    images = []
    num_image = (len(os.listdir(path)))
    for i in range(num_image):
        file_name = os.listdir(path)[i]
        img = io.imread(os.path.join(path, file_name))
        name=str(i)+'.tif'
        io.imsave('data/test2/'+name,img)

def gen_img(path):
    images = []
    num_image = (len(os.listdir(path)))
    for i in range(num_image):
        file_name = os.listdir(path)[i]
        img = io.imread(os.path.join(path, file_name))

        # crop
        height, width = img.shape[0], img.shape[1]
        left = 120 
        top = 7 
        right = width - left 
        bottom = height - top
        img = img[top:bottom, left:right, :]
        save_labels(img, i)
        print(i)

def gen_labels_black_and_white(path):
    images = gen_images(path)
    # for i in range(images.shape[0]):
    for k in range(images.shape[0]):
        img = images[k,0,:,:,:,0] * 255

        new = np.zeros((img.shape))
        for i in range(0, img.shape[0]):
                for j in range(0, img.shape[1]):
                    x,y,z = img[i,j,0],img[i,j,1],img[i,j,2]
                    # rgb colors:
                    # if red << green, it'c cyan
                    if ((y - x) > 30):
                        new[i,j,0] = 255
                        new[i,j,1] = 255
                        new[i,j,2] = 255
                    # if blue << green, it's yellow
                    elif ((y - z) > 30):
                        new[i,j,0] = 255
                        new[i,j,1] = 255
                        new[i,j,2] = 255
        save_labels(new, k)

def gen_labels_left_and_right(path):
    images = gen_images(path)
    img = images[150,0,:,:,:,0] * 255

    new = np.zeros((img.shape))
    for i in range(30, img.shape[0]):
            for j in range(30, img.shape[1]):
                x,y,z = img[i,j,0],img[i,j,1],img[i,j,2]
                # rgb colors:
                # if red << green, it'c cyan
                if ((y - x) > 30):
                    new[i,j,0] = 255
                # if blue << green, it's yellow
                elif ((y - z) > 30):
                    new[i,j,2] = 255
    save_labels(new)

def gen_images(path,target_size = (256,256),flag_multi_class=False):
    images = []
    num_image = (len(os.listdir(path)))
    for i in range(num_image):
        file_name = os.listdir(path)[i]
        img = io.imread(os.path.join(path, file_name))

        # crop
        height, width = img.shape[0], img.shape[1]
        left = 120 
        top = 7 
        right = width - left 
        bottom = height - top
        img = img[top:bottom, left:right, :]
        img = trans.resize(img,target_size)
        img = img * 255
        img = img.astype(np.uint8)
        img = np.reshape(img,img.shape+(1,)) if (not flag_multi_class) else img
        img = np.reshape(img,(1,)+img.shape)
        img = np.asarray(img)
        images.append(img)
        # yield img
    images = np.array(images)
    return images

def save_labels(img, i):
    fig, ax = plt.subplots()
    ax.imshow(img)
    ax.axis('image')
    ax.set_xticks([])
    ax.set_yticks([])
    title = "{}/{}.png".format(save_location, i)
    # plt.savefig("data/RednBlue/Test Results/test")
    plt.savefig(title)
    plt.close()

# gen_labels_black_and_white(img_location)
png_to_tiff()
