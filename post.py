import numpy as np 
import os
import glob
import skimage.io as io
import skimage.transform as trans
import matplotlib.pyplot as plt
import skimage.measure as measure
import statistics

result_location = "data/Test_C/Result"
target_location = "data/Test_C/Target"

# metrics to evaluate your semantic segmentation model

def get_vals(img, target):
    TP = 0      # True positive
    FP = 0      # False positive
    TN = 0      # True negative
    FN = 0      # False negative
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i,j] != 0:
                if target[i,j] != 0:
                    TP = TP + 1
                else:
                    FP = FP + 1
            else:
                if target[i,j] != 0:
                    FN = FN + 1
                else:
                    TN = TN + 1
    return TP, TN, FP, FN

def post():
    num_image = (len(os.listdir(result_location)))
    pixel_accuracies = []
    dice_coefficients = []
    IoUs = []
    for i in range(num_image):
        result_name = os.listdir(result_location)[i]
        result = io.imread(os.path.join(result_location, result_name))
        result = trans.resize(result, (256,256))
        target_name = os.listdir(target_location)[i]
        target = io.imread(os.path.join(target_location, target_name))
        target = trans.resize(target, (256,256))
        TP, TN, FP, FN = get_vals(result, target)
        # Pixel accuracy = correct / total
        pixel_accuracies.append((TP + TN) / (TP + FP + TN + FN))
        # IoU = mean of (overlap / union)
        IoUs.append(((TP / (FN + TP + FP)) + (TN / (FN + TN + FP))) / 2)
        # Dice coefficient = mean of (2 * overlap / total)
        dice1 = 2 * (TP) / (TP + FP + TP + FN)
        dice2 = 2 * (TN) / (TN + FP + TN + FN)
        dice_coefficients.append((dice1 + dice2) / 2)
    print(pixel_accuracies)
    print(IoUs)
    print(dice_coefficients)

# Results come out black and gray, change to black and white
def results_to_monochrome():
    path = "data/membrane/results"
    num_image = (len(os.listdir(path)))
    for i in range(num_image):
        file_name = os.listdir(path)[i]
        img = io.imread(os.path.join(path, file_name))
        new = np.zeros((img.shape), dtype='uint8')
        for j in range(img.shape[0]):
            for k in range(img.shape[1]):
                if img[j][k] > 30:
                    new[j][k] = 255
        name = str(i)+'.png'
        io.imsave('data/membrane/results/'+name,new)

# Target is 512x512, convert to 256x256
def target_to_size():
    path = "data/membrane/target"
    num_image = (len(os.listdir(path)))
    for i in range(num_image):
        file_name = os.listdir(path)[i]
        img = io.imread(os.path.join(path, file_name))
        new = np.zeros((256,256), dtype='uint8')
        for j in range(new.shape[0]):
            for k in range(new.shape[1]):
                new[j][k] = img[j*2][k*2] 
        name = str(i)+'.png'
        io.imsave('data/membrane/target/'+name,new)

# pixel_accuracies = [0.9916839599609375, 0.98681640625, 0.976959228515625, 0.9800872802734375, 0.9779510498046875, 0.9894256591796875, 0.9838205973307291]
# IoUs = [0.9808546299103437, 0.9669014485542131, 0.9408293494033386, 0.9469308642930541, 0.942146862280385, 0.9780797727197714, 0.9592904878601843]
# Dice_Coefficients = [0.9903217353772684, 0.9831106894809452, 0.9692839700977193, 0.9725404633838608, 0.9699794215817077, 0.9889128073701252, 0.9790248478819378]

a = [0.9046173095703125, 0.6859283447265625, 0.688720703125, 0.7597554524739584]
b = [0.8250765039706089, 0.5152358897828313, 0.519579794245467, 0.6199640626663024]
c = [0.9041112470520637, 0.678189352707349, 0.6822981863836135, 0.7548662620476754]
print(statistics.mean(a))
print(statistics.mean(b))
print(statistics.mean(c))

# post()
