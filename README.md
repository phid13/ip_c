This is an implementation of the convolutional neural network U-Net, along with various pre and post processing tools, used to segment lung geometries from a set of DICOM sections through the body.

The actual implementation of u-net is taken from https://github.com/zhixuhao/unet
