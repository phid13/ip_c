from model import *
from data import *

#os.environ["CUDA_VISIBLE_DEVICES"] = "0"


data_gen_args = dict(rotation_range=0.2,
                    width_shift_range=0.05,
                    height_shift_range=0.05,
                    shear_range=0.05,
                    zoom_range=0.05,
                    horizontal_flip=True,
                    fill_mode='nearest')
myGene = trainGenerator(2,'data/Train_C','Image','Label',data_gen_args,save_to_dir = None)

model = unet()
model_checkpoint = ModelCheckpoint('unet_membrane.hdf5', monitor='loss',verbose=1, save_best_only=True)
model.fit_generator(myGene,steps_per_epoch=300,epochs=4,callbacks=[model_checkpoint])

testGene = testGenerator("data/Test_C/Image")
results = model.predict_generator(testGene,30,verbose=1)
saveResult("data/Test_C/Result",results)

# loss = [1.2823,0.2358, 0.2168, 0.2119, 0.2026, 0.2030, 0.1958, 0.1921, 0.1864, 0.1858]
# accuracy = [0.8628, 0.9641, 0.9759, 0.9774, 0.9826, 0.9806, 0.9829, 0.9831, 0.9855, 0.9838]

# time = 561 seconds using Python 3 google compute backend gpu
